void main(){
  var mySet = new Set();
  mySet.add(100);
  mySet.add(102);
  mySet.add(103);
  mySet.add(104);
  mySet.add(104); // duplicate value don't print
  print(mySet);
  var mySet2 = {10, 20, 30, 40};
  print(mySet2);
  var myValue;
  for(myValue in mySet2){
    print(myValue);
  }
}