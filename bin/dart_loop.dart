void main(){
  // for loop
  for(var i=0; i<10; i++){
    print(i);
    print('Dart'); //print 10 times
    print('${i}Dart'); // concat with variable
    if(i==4){
      continue; // skipped 4
    }
    print('value of i-${i}');
    if(i==8){
      break;
    }
    print(i);
  }

  //while
  var i =0;
  while(i<10){
    print(i);
    i++;
  }

  //do while
  do{
    print(i);
    i++;
  }while(i<10);
}