import 'dart:collection'; // need to import this

void main(){
  var hashmap = new HashMap();
  hashmap['Name'] = 'Ashiq';
  hashmap['age'] = 23;
  print(hashmap);
  print(hashmap['Name']);
  print(hashmap.keys);
  print(hashmap.values);
  hashmap.clear();
  print(hashmap);
  hashmap['Name'] = 'Ashiq';
  hashmap['age'] = 23;
  print(hashmap);
  hashmap.remove('Name');
  print(hashmap);
  hashmap.addAll({'country':'BD', 'Address': 'Dhaka'});
  print(hashmap);
  var myvalues;
  for(myvalues in hashmap.values){
    print(myvalues);
  }

}