void main(){
  var myMap = {
    'Name' : 'Dipto',
    'Age' : 23,
    'Height' : 5.77
  };
  print(myMap);
  print(myMap['name']); // print by key
  myMap['Country'] = 'Bangladesh'; // add new [key] with 'value'
  print(myMap);

  var map = new Map(); //using constructor
  map['name'] = 'Dip';
  map['age'] = 20;
  print(map);
  print(map['name']);
  print(map.keys); //print only keys
  print(map.values); //print only values
  print(map.length); //print map length

  var map1 = {}; // new version assign map

  //addAll() method for map
  map1.addAll({'Name': 'Ashiq', 'Age': '20'});
  print(map1);
  //clear() method for map
  map1.clear(); //remove all values and keys
  print(map1);
  //remove() method for map
  map1.addAll({'Name': 'Ashiq', 'Age': '20'});
  map1.remove('Name');
  print(map1);
}
