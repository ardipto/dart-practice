import 'inherit_father.dart';

class Son extends Father{
  void sum(){
    super.mulTwo();
  }
  void sonMethod(){
    print(20);
  }
  @override
  void addTwo(){ // method overriding
    print(20+30+10);
  }

}