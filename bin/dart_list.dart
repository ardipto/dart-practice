void main(){
  var myFixedList = new List(3); // fixed list
  var myGrowableList = new List(); // Growable list

  myFixedList[0] = 'Ashiqur';
  myFixedList[1] = 'Rahaman';
  myFixedList[2] = 'Dipto';

  print(myFixedList);

  myGrowableList.add('Ashik'); //add to list
  myGrowableList.add('Rahaman');
  myGrowableList.add('Dipto');
  myGrowableList.add('hello');
  myGrowableList.remove('hello'); //remove from list

  print(myGrowableList);

  var list = []; //new version of list
  list.add('value');
  list.addAll(['value1', 'value2']); // add all value
  list.insert(1, 10); // need to mansion index number
  list.insertAll(1, [20, 'age']); // insert all with index number
  list.remove(10); // remove with value
  list.removeAt(0); // remove with index
  print(list);

  var list1 = [];
  list1.addAll([1,2, 3, 5, 6]);
  list1.replaceRange(1, 2, ['three']); // starting, ending, [value] replace with index
  print(list1);
}